package org.example;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class TemperatureFact {
    /** Temperature Degree in Celsius */
    Integer degrees;

    @Override
    public String toString() {
        return "TemperatureFact - degrees: " + degrees.toString();
    }
}
