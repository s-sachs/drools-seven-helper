package org.example;

import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.rule.FactHandle;

import java.util.Collection;

public class Main {

    public static void main(String[] args) {
        Main main = new Main();
        main.executeRules();
    }

    public void executeRules() {
        // Setup Kie Environment
        KieServices kieServices = KieServices.Factory.get();
        KieContainer kieContainer = kieServices.getKieClasspathContainer();
        KieSession session = kieContainer.newKieSession();

        try {
            // Setup Facts
            // - Add your input facts here
            TemperatureFact customFact = new TemperatureFact(-3);
            session.insert(customFact);

            // Print Facts before firing
            System.out.println("FactHandles Input");
            Collection<FactHandle> factHandleList = session.getFactHandles();
            factHandleList.forEach(System.out::println);

            // Fire Rules
            session.fireAllRules();

            // Print Facts after firing
            System.out.println("FactHandles Output");
            factHandleList = session.getFactHandles();
            factHandleList.forEach(System.out::println);

        } catch (Exception e) {
            // Skip error handling
        } finally {
            // Close session
            session.dispose();
        }
    }


}
