package org.example;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CustomFact {
    /** Key string*/
    String key;

    /** Value string*/
    String value;

    @Override
    public String toString() {
        return "CustomFact - key: \"" + key + "\", value: \"" + value + "\"";
    }
}
