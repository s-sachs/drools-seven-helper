# drools-seven-helper

This is a dummy project to be able to create and test Drools 7.73.0.Final rules.

## Usage

### Facts
In `/src/main/java/org/example/Main.class` you can add the input facts that are to be used by your rules.

If you want a custom fact, create a new class just like the `CustomFact.class`.
Make sure to add getters _(lombok ftw)_, as Drools uses them to read the facts values in the conditions.

### Rules
In `/src/main/resources/org/example/` you can add your drools rule.

Make sure to add the same package name as your facts are in `org.example`. 

### Hints
Make sure that your rule and the models are in the same package.
Otherwise, Drools won't be able to identify the Facts used in the rules.

Please note that this is a very basic execution scheme.
It does not take into account things like activation groups or execution of the first matching rule only!  

## Help
This project was created based on this SO question:
https://stackoverflow.com/questions/39504729/how-to-configure-drools-without-eclipse-or-any-other-ide